<?php
/**
 * Template Name: Alle Projecten
 * The template for displaying all example projects
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Boerderijenfonds_Theme
 */

get_header();

$color_page = get_post_meta(get_the_ID(), 'meta-page-color', true);
$color_description = get_post_meta(get_the_ID(), 'meta-description-color', true);
$color_content = get_post_meta(get_the_ID(), 'meta-content-color', true);
?>
	
<section class="excerpt">
	<div class="section-inner excerpt">
		<div class="row">
			<div class="col description">
				<?php if(has_excerpt() ) { the_excerpt(); } ?>
			</div>
		</div>
	</div>
</section>	
<section>
	<div class="section-inner projecten-archive <?php echo $color_content; ?>">
		<?php archive_projects(); ?>
	</div>
</section>
<?php
get_footer();
