<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Boerderijenfonds_Theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<section>
			<div class="section-inner">
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<h4></h4>
						<p>
							<a href="mailto:bureau@boerderijenfonds.nl">bureau@boerderijenfonds.nl</a><br>
							<a href="https://www.boerderijenfonds.nl">www.boerderijenfonds.nl</a><br>
								06 572 434 16 (di-ochtend/don-ochtend)<br>
								06 578 128 45 (wo)</p>
								<p>
								<a href="https://www.facebook.com/boerderijinbeeld" target="_blank">
									<img src="/wp-content/themes/boerderijenfonds/images/facebook-f-brands.svg" width="15" alt="Facebook" />
									</a>

						</p>
					</div>
					<div class="col-sm-8 col-xs-12">
					<h4 onclick="showDiv('colofon')" style="cursor:pointer;text-decoration:underline;">Colofon</h4>
					<div style="display:none" id="colofon">
						<p>Deze website is ontworpen door <a href="http://aukjegorter.nl">Aukje Gorter grafisch ontwerper</a>
						en <a href="https://zoo.nl">studio ZOO</a>, in opdracht van stichting Boerderijenfonds.<br>
						Foto’s: AEN en boerderijenstichtingen, tenzij anders vermeld; Harry Cock, Erwin Muntinga<br>
						Teksten: Boerderijenfonds<br>
						&copy; Boerderijenfonds, KvK 77260228
						</p>
						<p>
							Auteursrecht<br>
							Alle afbeeldingen op Boerderijenfonds.nl zijn auteursrechtelijk beschermd en mogen niet zonder 
							toestemming worden gebruikt door anderen. Staat er een tekst of afbeelding op deze website waarvan 
							verkeerde copyrights worden gebruikt, wilt u dit dan melden via <a href="mailto:bureau@boerderijenfonds.nl">bureau@boerderijenfonds.nl</a>?
						</p>
					</div>
					<h4 onclick="showDiv('disclaimer')" style="cursor:pointer;text-decoration:underline;">Disclaimer</h4>
					<p style="display:none;" id="disclaimer"> 	
					Op deze website van het Boerderijenfonds, www.boerderijenfonds.nl, vindt u onder meer informatie over de inrichting 
					van het fonds en over richtlijnen, beoordelingscriteria en procedure met betrekking tot het indienen van aanvragen 
					voor het verkrijgen van een financiële bijdrage of advies. Deze en overige op deze website opgenomen informatie is 
					uitsluitend bedoeld als algemene informatie. Er kunnen geen rechten aan deze informatie worden ontleend. 
					<br><br>
					Ondanks de constante zorg en aandacht die het Boerderijenfonds aan de samenstelling van deze website besteedt, 
					bestaat er de kans dat de informatie die hier wordt gepubliceerd onvolledig, niet actueel of onjuist is. Het 
					Boerderijenfonds kan evenmin garanderen dat de website te allen tijde en in elke situatie foutloos en ononderbroken 
					zal functioneren.<br><br>
					Het Boerderijenfonds sluit alle aansprakelijkheid uit voor enigerlei directe of indirecte schade, van welke aard dan 
					ook, die voortvloeit uit, of in enig opzicht verband houdt met, het gebruik van deze website, of die het gevolg is van
					de tijdelijke onmogelijkheid om de website te raadplegen. Het Boerderijenfonds is ook niet aansprakelijk voor directe
					of indirecte schade die het gevolg is van het gebruik van informatie die door middel van deze website verkregen is.<br>
					De informatie op deze website wordt regelmatig aangevuld, gewijzigd of geactualiseerd. Dergelijke aanpassingen kunnen 
					te allen tijde met onmiddellijke ingang en zonder enige kennisgeving worden aangebracht. Voor vragen met betrekking 
					tot aansprakelijkheidsvraagstukken kunt u een e-mailbericht sturen aan <a href="mailto:bureau@boerderijenfonds.nl">bureau@boerderijenfonds.nl</a>.
</p>
	
					<h4><a href="/privacyverklaring/">Privacy verklaring</a></h4>
					</div>
				</div>

			</div><!-- .site-info -->
		</section>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script>
function showDiv(e) {
  var x = document.getElementById(e);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>

</body>
</html>
