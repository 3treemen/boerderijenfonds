<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Boerderijenfonds_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="title" content="<?php echo get_bloginfo('name'); ?>">
	<meta name="description" content="<?php echo get_bloginfo('description');?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
	<link rel="preload" href="https://fonts.gstatic.com/s/asap/v11/KFOoCniXp96ayzse4A.woff2" as="font" crossorigin>
	<link rel="preload" href="https://fonts.gstatic.com/s/asap/v11/KFOnCniXp96aw4A79UtvBg.woff2" as="font" crossorigin>
	<link rel="preload" href="https://boerderijenfonds.nl/wp-content/themes/boerderijenfonds/fonts/SansaCondensed-Light.woff2" as="font" crossorigin>
	<link rel="preload" href="https://boerderijenfonds.nl/wp-content/themes/boerderijenfonds/fonts/SansaCondensed-Normal.woff2" as="font" crossorigin>
	<link rel="preload" href="https://boerderijenfonds.nl/wp-content/themes/boerderijenfonds/fonts/SansaCondensed-SemiBold.woff2" as="font" crossorigin>
	<style>
		@font-face {
			font-family: 'Asap';
			font-style: normal;
			font-weight: 400;
			font-display: swap;
			src: local('Asap Regular'), local('Asap-Regular'), url(https://fonts.gstatic.com/s/asap/v11/KFOoCniXp96ayzse4A.woff2) format('woff2');
			unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		}
		@font-face {
			font-family: 'Asap';
			font-style: normal;
			font-weight: 700;
			font-display: swap;
			src: local('Asap Bold'), local('Asap-Bold'), url(https://fonts.gstatic.com/s/asap/v11/KFOnCniXp96aw4A79UtvBg.woff2) format('woff2');
			unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		}
		@font-face {
			font-family: 'Sansa Condensed';
			font-style: light;
			font-weight: 200;
			src: local('SansaCondensed Light'), local('SansaCondensed-Light'), url(https://boerderijenfonds.nl/wp-content/themes/boerderijenfonds/fonts/SansaCondensed-Light.woff2) format('woff2');
			unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		}
		@font-face {
			font-family: 'Sansa Condensed';
			font-style: bold;
			font-weight: 600;
			src: local('SansaCondensed SemiBold'), local('SansaCondensed-SemiBold'), url(https://boerderijenfonds.nl/wp-content/themes/boerderijenfonds/fonts/SansaCondensed-SemiBold.woff2) format('woff2');
			unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		}
		@font-face {
			font-family: 'Sansa Condensed';
			font-style: normal;
			font-weight: 400;
			src: local('SansaCondensed Normal'), local('SansaCondensed-Normal'), url(https://boerderijenfonds.nl/wp-content/themes/boerderijenfonds/fonts/SansaCondensed-Normal.woff2) format('woff2');
			unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		}

	</style>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'boerderijenfonds' ); ?></a>

	<section id="masthead" class="site-header">
		<div class="section-inner">
			<div class="row ">	
				<div class="col-xs-6 col-sm-3 col site-branding">
					<a href="/">
					<img src="/wp-content/themes/boerderijenfonds/images/LogoBoerderijenfonds-web_scaled.png" 
						alt="Boerderijenfonds" id="logo" />
					</a>
				</div><!-- .site-branding -->
				<div class="col-xs-6 col-sm-9">
					<div class="row">
						<div class="col-xs-12">
							<?php 
								wp_nav_menu( array( 
									'theme_location' => 'top-menu',
									'menu_id' => 'top-menu',
								));
							?>
						</div>
						<div class="col-xs-12 pt0">
							<nav id="site-navigation" class="align-right main-navigation">
								
								<button class="menu-toggle align-right" aria-controls="primary-menu" aria-expanded="false">
								<img src="/wp-content/themes/boerderijenfonds/images/menu.svg" alt="menu" />
								</button>
								<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
								) );
								?>
							</nav><!-- #site-navigation -->
						</div>
					</div>
				</div>
			</div>
		</div>		
	</section><!-- #masthead -->
<?php 
if ( is_singular( array( 'page', 'project' ) ) ) {
    //  conditional content/code
	$slide_show_code = get_post_meta(get_the_ID(), 'slide-show-code', true);
if ( get_post_type() != 'project' ) {
?>
	<section id="content">
		<div class="row">
			<div class="col col-no-gutter">
				<?php
				if($slide_show_code) {
					echo do_shortcode($slide_show_code); 
				} 
				else {
					boerderijenfonds_post_thumbnail();
				} 
				?>
			</div>
		</div>
	</section>
	<?php 
	}
}
if(!is_front_page() ) {
	if( get_post_type() === 'bib' || is_page_template( 'archive-bib.php'))  {
		include 'template-parts/header-bib.php';
	}
	elseif( get_post_type() === 'project' ) {
		include 'template-parts/header-project.php';
	}
	else {
		include 'template-parts/header-default.php';
	}
}
?>