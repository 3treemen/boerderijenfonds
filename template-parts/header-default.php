<?php 
$color_header = get_post_meta(get_the_ID(), 'meta-header-color', true);
?>
<section id="header" class="<?php echo $color_header; ?>">
	<div class="section-inner">
		<div class="row">
			<div class="col">
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                <?php 
					if(function_exists('wpd_nav_menu_breadcrumbs')): 
							echo "<a href='/'>Home</a> > ";
								wpd_nav_menu_breadcrumbs( 'mainMenu' );
								wpd_nav_menu_breadcrumbs( 'Top Menu' );
								
								if ( is_page_template( 'archive.php' ) ) {
									echo "Nieuwsarchief";
								} 
								
								elseif ( get_current_template() == 'single.php'  ) {
									echo "<a href='/nieuwsarchief/'>Nieuwsarchief</a> > ";
									echo the_title();
								}  
								
								else {
								}			
						endif;
						
					?>
				</div>
			</div>
		</div>
	</div>
</section>
