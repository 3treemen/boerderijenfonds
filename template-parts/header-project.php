<?php 
$color_header = get_post_meta(get_the_ID(), 'meta-header-color', true);
?>
<section id="header" class="<?php echo $color_header; ?>">
	<div class="section-inner">
		<div class="row">
			<div class="col">
				<header class="entry-header">
					<h1>Projecten</h1>
				</header><!-- .entry-header -->
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				<?php 
					// if(function_exists('wpd_nav_menu_breadcrumbs')): 
						echo "<a href='/'>Home</a> > <a href='/projecten'>Projecten</a> > ";
						echo the_title();
						// wpd_nav_menu_breadcrumbs( 'mainMenu' );  
					// endif;
				?>
				</div>
			</div>
		</div>
	</div>
</section>