<?php 
$color_header = get_post_meta(get_the_ID(), 'meta-header-color', true);
?>
<section id="header" class="<?php echo $color_header; ?>">
	<div class="section-inner">
		<div class="row">
			<div class="col">
				<header class="entry-header">
						<img src="/wp-content/themes/boerderijenfonds/images/Logo-BiB.png" alt="Boerderij in Beweging" />
				</header><!-- .entry-header -->
				</div>
			</div>
		</div>
	</div>
</section>
