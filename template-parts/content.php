<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Boerderijenfonds_Theme
 */
$date = get_the_date();
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'large');

if(has_excerpt() ) {
?>
<section 
	<?php 
		if($color_description) { 
			echo "class='" . $color_description . "'"; 
			} 
	?>
>
		<div class="section-inner">
			<div class="row">
				<div class="col">
					<div class="col description">
						<?php the_excerpt(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } ?>
<section class="light-green">
	<div class="section-inner small_col">
		<article class="white" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="row">
				<div class="col">		

					<div class="entry-content">
						<?php
						if(has_post_thumbnail()) {
							the_post_thumbnail('large',array('class' => 'ml1'));
						} 
						?>
						<p class="date"><?php echo $date; ?></p>
						<?php
						the_content( sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'boerderijenfonds' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						) );

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'boerderijenfonds' ),
							'after'  => '</div>',
						) );
						?>
					</div><!-- .entry-content -->
				</div>
			</div>

		</article><!-- #post-<?php the_ID(); ?> -->
	</div><!-- // container -->
</section>