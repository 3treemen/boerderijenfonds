<?php
/**
 * Template part for displaying project content in single-project.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Boerderijenfonds_Theme
 */


$color_page = get_post_meta(get_the_ID(), 'meta-page-color', true);
$color_description = get_post_meta(get_the_ID(), 'meta-description-color', true);
$color_content = get_post_meta(get_the_ID(), 'meta-content-color', true);
$slideshow = get_post_meta(get_the_ID(), 'meta-slider', true);

$meta = get_post_meta(get_the_ID(), '', true);
$meta_use = get_post_meta(get_the_ID(), 'meta-use', true);
$meta_website = get_post_meta(get_the_ID(), 'meta-website', true);
$meta_budget = get_post_meta(get_the_ID(), 'meta-budget', true);
$meta_ervaring = get_post_meta(get_the_ID(), 'meta-ervaring', true);
?>

<section class="content">
	<div class="section-inner">
		<?php if($slideshow) {
			echo do_shortcode($slideshow);
		}?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content">
				<h2><?php echo the_title(); ?></h2>
				<?php if($meta_use) {?>
					<div class="psub">
						<h3>Gebruik</h2>	
						<p class="mt0"><?php echo $meta_use; ?></p>
					</div>		
				<?php } ?>	
				<div class="psub">
					<h3>Projectinformatie</h3>
					<div class="projectcontent">
						<?php echo get_the_content();?>
					</div>
				</div>
				<?php if($meta_budget) {?>
					<div class="psub">
						<h3>Budget</h3>
						<p class="mt0"><?php echo $meta_budget; ?></p>
				<?php } ?>
			

				<?php if($meta_website) {?>
					<div class="psub">
						<h3>Website</h3>
						<p class="mt0"><a href="<?php echo $meta_website; ?>" target="_blank"><?php echo $meta_website; ?></a></p>	
					</div>
				<?php } ?>
			</div><!-- .entry-content -->

		</article><!-- #post-<?php the_ID(); ?> -->
	</div>
</section>