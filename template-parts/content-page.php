<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Boerderijenfonds_Theme
 */

$page_extra_class= get_post_meta(get_the_ID(), 'meta-page-extra', true);
$color_page = get_post_meta(get_the_ID(), 'meta-page-color', true);
$color_description = get_post_meta(get_the_ID(), 'meta-description-color', true);
$color_content = get_post_meta(get_the_ID(), 'meta-content-color', true);

if(has_excerpt() ) { ?>

<section <?php if($color_description) { 
	echo "class='excerpt " . $color_description . "'"; 
	} 
	?>
	>
	<div class="section-inner <?php if($page_extra_class) { echo $page_extra_class; } ?>">
		<div class="row">
			<div class="col">
				<?php the_excerpt();  ?>
			</div>
		</div>
	</div>
</section>
	<?php } ?>
<section <?php if($color_page) { echo "class='" . $color_page . "'";} ?>>
	<div class="section-inner <?php if($page_extra_class) { echo $page_extra_class; } ?>">
		<article id="post-<?php the_ID(); ?>" <?php if($color_content) { echo "class='" . $color_content . "'";} ?>>
			<div class="row">
				<div class="col">
				<?php
				the_content();

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'boerderijenfonds' ),
					'after'  => '</div>',
				) );
				?>
			</div><!-- .entry-content -->
					</div>
			<?php if ( get_edit_post_link() ) : ?>
				<footer class="entry-footer">
					<?php
					edit_post_link(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Edit <span class="screen-reader-text">%s</span>', 'boerderijenfonds' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						),
						'<span class="edit-link">',
						'</span>'
					);
					?>
				</footer><!-- .entry-footer -->
			<?php endif; ?>
		</article><!-- #post-<?php the_ID(); ?> -->
	</div>
</div>