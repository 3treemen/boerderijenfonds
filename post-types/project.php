<?php

/**
 * Registers the `project` post type.
 */
function project_init() {
	register_post_type( 'project', array(
		'labels'                => array(
			'name'                  => __( 'Projecten', 'boerderijenfonds' ),
			'singular_name'         => __( 'Projecten', 'boerderijenfonds' ),
			'all_items'             => __( 'All Projects', 'boerderijenfonds' ),
			'archives'              => __( 'Project Archives', 'boerderijenfonds' ),
			'attributes'            => __( 'Project Attributes', 'boerderijenfonds' ),
			'insert_into_item'      => __( 'Insert into Project', 'boerderijenfonds' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Project', 'boerderijenfonds' ),
			'featured_image'        => _x( 'Featured Image', 'project', 'boerderijenfonds' ),
			'set_featured_image'    => _x( 'Set featured image', 'project', 'boerderijenfonds' ),
			'remove_featured_image' => _x( 'Remove featured image', 'project', 'boerderijenfonds' ),
			'use_featured_image'    => _x( 'Use as featured image', 'project', 'boerderijenfonds' ),
			'filter_items_list'     => __( 'Filter Projects list', 'boerderijenfonds' ),
			'items_list_navigation' => __( 'Projects list navigation', 'boerderijenfonds' ),
			'items_list'            => __( 'Projects list', 'boerderijenfonds' ),
			'new_item'              => __( 'New Project', 'boerderijenfonds' ),
			'add_new'               => __( 'Add New', 'boerderijenfonds' ),
			'add_new_item'          => __( 'Add New Project', 'boerderijenfonds' ),
			'edit_item'             => __( 'Edit Project', 'boerderijenfonds' ),
			'view_item'             => __( 'View Project', 'boerderijenfonds' ),
			'view_items'            => __( 'View Projects', 'boerderijenfonds' ),
			'search_items'          => __( 'Search Projects', 'boerderijenfonds' ),
			'not_found'             => __( 'No Projects found', 'boerderijenfonds' ),
			'not_found_in_trash'    => __( 'No Projects found in trash', 'boerderijenfonds' ),
			'parent_item_colon'     => __( 'Parent Project:', 'boerderijenfonds' ),
			'menu_name'             => __( 'Projecten', 'boerderijenfonds' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' ),
		'has_archive'           => false,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'projecten',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		// 'taxonomies'          => array( 'category' ),
	) );

}
add_action( 'init', 'project_init' );

/**
 * Sets the post updated messages for the `project` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `project` post type.
 */
function project_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['project'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Project updated. <a target="_blank" href="%s">View Project</a>', 'boerderijenfonds' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'boerderijenfonds' ),
		3  => __( 'Custom field deleted.', 'boerderijenfonds' ),
		4  => __( 'Project updated.', 'boerderijenfonds' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Project restored to revision from %s', 'boerderijenfonds' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Project published. <a href="%s">View Project</a>', 'boerderijenfonds' ), esc_url( $permalink ) ),
		7  => __( 'Project saved.', 'boerderijenfonds' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Project submitted. <a target="_blank" href="%s">Preview Project</a>', 'boerderijenfonds' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Project</a>', 'boerderijenfonds' ),
		date_i18n( __( 'M j, Y @ G:i', 'boerderijenfonds' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Project draft updated. <a target="_blank" href="%s">Preview Project</a>', 'boerderijenfonds' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'project_updated_messages' );
