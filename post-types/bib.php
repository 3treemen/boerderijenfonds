<?php

/**
 * Registers the `bib` post type.
 */
function bib_init() {
	register_post_type( 'bib', array(
		'labels'                => array(
			'name'                  => __( 'Boerderij in beweging', 'boerderijenfonds' ),
			'singular_name'         => __( 'Boerderij in beweging', 'boerderijenfonds' ),
			'all_items'             => __( 'All Boerderij in beweging', 'boerderijenfonds' ),
			'archives'              => __( 'Boerderij in beweging Archives', 'boerderijenfonds' ),
			'attributes'            => __( 'Boerderij in beweging Attributes', 'boerderijenfonds' ),
			'insert_into_item'      => __( 'Insert into Boerderij in beweging', 'boerderijenfonds' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Boerderij in beweging', 'boerderijenfonds' ),
			'featured_image'        => _x( 'Featured Image', 'bib', 'boerderijenfonds' ),
			'set_featured_image'    => _x( 'Set featured image', 'bib', 'boerderijenfonds' ),
			'remove_featured_image' => _x( 'Remove featured image', 'bib', 'boerderijenfonds' ),
			'use_featured_image'    => _x( 'Use as featured image', 'bib', 'boerderijenfonds' ),
			'filter_items_list'     => __( 'Filter Boerderij in beweging list', 'boerderijenfonds' ),
			'items_list_navigation' => __( 'Boerderij in beweging list navigation', 'boerderijenfonds' ),
			'items_list'            => __( 'Boerderij in beweging list', 'boerderijenfonds' ),
			'new_item'              => __( 'New Boerderij in beweging', 'boerderijenfonds' ),
			'add_new'               => __( 'Add New', 'boerderijenfonds' ),
			'add_new_item'          => __( 'Add New Boerderij in beweging', 'boerderijenfonds' ),
			'edit_item'             => __( 'Edit Boerderij in beweging', 'boerderijenfonds' ),
			'view_item'             => __( 'View Boerderij in beweging', 'boerderijenfonds' ),
			'view_items'            => __( 'View Boerderij in beweging', 'boerderijenfonds' ),
			'search_items'          => __( 'Search Boerderij in beweging', 'boerderijenfonds' ),
			'not_found'             => __( 'No Boerderij in beweging found', 'boerderijenfonds' ),
			'not_found_in_trash'    => __( 'No Boerderij in beweging found in trash', 'boerderijenfonds' ),
			'parent_item_colon'     => __( 'Parent Boerderij in beweging:', 'boerderijenfonds' ),
			'menu_name'             => __( 'Boerderij in beweging', 'boerderijenfonds' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'bib',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'bib_init' );

/**
 * Sets the post updated messages for the `bib` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `bib` post type.
 */
function bib_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['bib'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Boerderij in beweging updated. <a target="_blank" href="%s">View Boerderij in beweging</a>', 'boerderijenfonds' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'boerderijenfonds' ),
		3  => __( 'Custom field deleted.', 'boerderijenfonds' ),
		4  => __( 'Boerderij in beweging updated.', 'boerderijenfonds' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Boerderij in beweging restored to revision from %s', 'boerderijenfonds' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Boerderij in beweging published. <a href="%s">View Boerderij in beweging</a>', 'boerderijenfonds' ), esc_url( $permalink ) ),
		7  => __( 'Boerderij in beweging saved.', 'boerderijenfonds' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Boerderij in beweging submitted. <a target="_blank" href="%s">Preview Boerderij in beweging</a>', 'boerderijenfonds' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Boerderij in beweging scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Boerderij in beweging</a>', 'boerderijenfonds' ),
		date_i18n( __( 'M j, Y @ G:i', 'boerderijenfonds' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Boerderij in beweging draft updated. <a target="_blank" href="%s">Preview Boerderij in beweging</a>', 'boerderijenfonds' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'bib_updated_messages' );
