<?php
/**
 * Template Name: Nieuws Archief
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Boerderijenfonds_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<section class="light-blue">
			<div class="section-inner small_col newslist">
				<div class="white">
					<?php nieuws_posts('archive'); ?>
				</div>
			</div>
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
