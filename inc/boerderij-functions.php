<?php
/**
 * Boerderij specific functions
 *
 * @package Boerderijenfonds_Theme
 */
add_post_type_support( 'page', 'excerpt' );

function register_extra_menus() {
	register_nav_menu('top-menu',__( 'Top Menu' ));
	register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_extra_menus' );


// add meta fields to project
function add_project_meta_box() {
    add_meta_box(
        'project_meta_box', //$id
        'Project', // $title 
        'project_meta_box_markup', // $callback
        'project', // $screen
        'side', // $context
        'high', // $priority
        null
    );
}
add_action( 'add_meta_boxes', 'add_project_meta_box' );


// create the input fields
function project_meta_box_markup($object) {
	$meta = get_post_meta( $object->ID );
	wp_nonce_field(basename(__FILE__), "meta-box-nonce");
	?>
	<p>
        <label for="meta-website" class="boerderijenfonds-row-title"><?php _e( 'website', 'boerderijenfonds' )?></label>
        <input type="text" name="meta-website" id="meta-website" value="<?php if ( isset ( $meta['meta-website'] ) ) echo $meta['meta-website'][0]; ?>" />
    </p>
	<p>
        <label for="meta-use" class="boerderijenfonds-row-title"><?php _e( 'Gebruik', 'boerderijenfonds' )?></label>
        <input type="text" name="meta-use" id="meta-use" value="<?php if ( isset ( $meta['meta-use'] ) ) echo $meta['meta-use'][0]; ?>" />
    </p>
	<p>
        <label for="meta-budget" class="boerderijenfonds-row-title"><?php _e( 'Budget', 'boerderijenfonds' )?></label>
        <input type="text" name="meta-budget" id="meta-budget" value="<?php if ( isset ( $meta['meta-budget'] ) ) echo $meta['meta-budget'][0]; ?>" />
	</p>
	<p>
        <label for="meta-slider" class="boerderijenfonds-row-title"><?php _e( 'Slideshow code', 'boerderijenfonds' )?></label>
        <input type="text" name="meta-slider" id="meta-slider" value="<?php if ( isset ( $meta['meta-slider'] ) ) echo esc_attr($meta['meta-slider'][0]); ?>" />
    </p>
	
<?php
}


// save the fields on update page
function save_project_meta_box( $post_id ) {
	
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'prfx_nonce' ] ) && wp_verify_nonce( $_POST[ 'prfx_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
	
	if( isset( $_POST[ 'meta-website' ] ) ) {
        update_post_meta( $post_id, 'meta-website', sanitize_text_field( $_POST[ 'meta-website' ] ) );
	}

	if( isset( $_POST[ 'meta-use' ] ) ) {
        update_post_meta( $post_id, 'meta-use', sanitize_text_field( $_POST[ 'meta-use' ] ) );
	}
	
	if( isset( $_POST[ 'meta-budget' ] ) ) {
        update_post_meta( $post_id, 'meta-budget', sanitize_text_field( $_POST[ 'meta-budget' ] ) );
	}
	
	if( isset( $_POST[ 'meta-slider' ] ) ) {
        update_post_meta( $post_id, 'meta-slider', sanitize_text_field( $_POST[ 'meta-slider' ] ) );
    }
}
add_action("save_post", "save_project_meta_box");



function add_page_meta_box() {
	add_meta_box(
		'page_meta',
		'custom',
		'page_meta_markup',
		'page',
		'side',
		'high',
		null
	);
}
add_action( 'add_meta_boxes', 'add_page_meta_box');

function page_meta_markup($object) {
	$meta = get_post_meta( $object->ID );
	wp_nonce_field(basename(__FILE__), "meta-box-nonce");
	?>
	<div>
		<label for="meta-page-extra">Pagina classes 
			<input name="meta-page-extra" type="text" 
				value="<?php echo get_post_meta($object->ID, "meta-page-extra", true); ?>">
		</label>

	</div>
	<div>
		<label for="meta-header-color">Achtergrondkleur kop 
			<input name="meta-header-color" type="text" 
				value="<?php echo get_post_meta($object->ID, "meta-header-color", true); ?>">
		</label>
	</div>

	<div>
		<label for="meta-page-color">Achtergrondkleur pagina 
			<input name="meta-page-color" type="text" 
				value="<?php echo get_post_meta($object->ID, "meta-page-color", true); ?>">
		</label>
	</div>

	<div>
		<label for="meta-description-color">Achtergrondkleur omschrijving
			<input name="meta-description-color" type="text" 
				value="<?php echo get_post_meta($object->ID, "meta-description-color", true); ?>">
		</label>
	</div>
	<div>
		<label for="meta-content-color">Achtergrondkleur content
			<input name="meta-content-color" type="text" 
				value="<?php echo get_post_meta($object->ID, "meta-content-color", true); ?>">
		</label>
	</div>
	
	

	<?php
}

function save_page_meta_box($post_id, $post, $update) {
	if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
	return $post_id;

	if(!current_user_can("edit_post", $post_id))
		return $post_id;

	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
		return $post_id;

	$meta_page_extra_value = "";
	$meta_header_color_value = "";
	$meta_page_color_value = "";
	$meta_description_color_value = "";
	$meta_content_color_value = "";

	if(isset($_POST["meta-page-extra"])) {
		$meta_page_extra_value = $_POST["meta-page-extra"];
	}
	update_post_meta($post_id, "meta-page-extra", $meta_page_extra_value);

	if(isset($_POST["meta-header-color"])) {
		$meta_header_color_value = $_POST["meta-header-color"];
	}
	update_post_meta($post_id, "meta-header-color", $meta_header_color_value);

	if(isset($_POST["meta-page-color"])) {
		$meta_page_color_value = $_POST["meta-page-color"];
	}
	update_post_meta($post_id, "meta-page-color", $meta_page_color_value);

	if(isset($_POST["meta-description-color"])) {
		$meta_description_color_value = $_POST["meta-description-color"];
	}
	update_post_meta($post_id, "meta-description-color", $meta_description_color_value);

	if(isset($_POST["meta-content-color"])) {
		$meta_content_color_value = $_POST["meta-content-color"];
	}
	update_post_meta($post_id, "meta-content-color", $meta_content_color_value);

}
add_action("save_post", "save_page_meta_box", 10, 4);


// get the voorbeeld projecten
function archive_projects() {
	$args = array(
		'post_type' => 'project',
		'post_status' => 'publish',
		'orderby' => 'publish_date',
		'order' => 'DESC',
		'posts_per_page' => -1,
	);

	$query = new WP_Query( $args );
	if( $query->have_posts() ) {
		while ($query->have_posts() ) {
			$query->the_post();
			$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
			$permalink = get_permalink();
			$use = get_post_meta(get_the_ID(), 'meta-use', true);
			echo "<div class='project'>";
			echo "<a href='". $permalink . "'>";
			if ( has_post_thumbnail() ) {				
				$alt = get_post_meta ( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
				echo '<img src="'. $featured_img_url . '" alt="'. $alt .'" style="width:100%" />';
				// the_post_thumbnail('medium',['class' => 'responsive', 'title' => esc_html ( $alt ),  'alt' => esc_html ( $alt ) ]);
			}
			echo the_title('<b>', '</b>');
			echo "<p>" . $use . "</p>";
			echo "</a>";
			echo "</div>";
		}
	}
	wp_reset_postdata();
}
add_shortcode('vbprojecten', 'example_projects');


// get bib pages
function archive_bib() {
	$args = array(
		'post_type' => 'bib',
		'post_status' => 'publish',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
	);

	$query = new WP_Query( $args );
	if( $query->have_posts() ) {
		while ($query->have_posts() ) {
			$query->the_post();
			$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
			$permalink = get_permalink();
			$use = get_post_meta(get_the_ID(), 'meta-use', true);
			echo "<div class='project'>";
			echo "<a href='". $permalink . "'>";
			if ( has_post_thumbnail() ) {				
				$alt = get_post_meta ( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
				echo '<img src="'. $featured_img_url . '" alt="'. $alt .'" style="width:100%" />';
				// the_post_thumbnail('medium',['class' => 'responsive', 'title' => esc_html ( $alt ),  'alt' => esc_html ( $alt ) ]);
			}
			echo the_title('<b>', '</b>');
			echo "<p>" . $use . "</p>";
			echo "</a>";
			echo "</div>";
		}
	}
	wp_reset_postdata();
}
add_shortcode('bib', 'bib_pages');


// get the recent post, widget is not sufficient
function nieuws_posts($template) {
	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC'
	);

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		$count = 0;
		while ( $query->have_posts() ){
			$count++;
			$query->the_post();
			$permalink = get_permalink();
			$date = get_the_date();
			if($template == 'homepage') {
				if($count >= 1 && $count <= 3 ) {
					echo "<div class='newsitem'>";
					if($count == 1 ) {
						if(has_post_thumbnail()) {
							the_post_thumbnail('medium',array('class' => 'ml1'));
						} 
					}
					echo "<a href='". $permalink ."'>";
					echo the_title('<h5>', '</h5>');
					echo "<span class='date'>";
					echo $date;
					echo "</span>";
					echo "<p>" . get_the_excerpt() . "</p>";
					echo "<div class='small italic pl1'>";
					echo "lees meer..." ;
					echo "</div>";
					echo "</a></div><hr>";	
				}
			}
			else if($template == 'archive') {
				echo "<div class='item'>";
				echo "<a href='". $permalink ."'>";
				echo the_title('<h5>', '</h5>');
				echo "<div class='date'>" . $date . "</div>";
				echo "</a>";
				echo "</div>";
			}
		}
		if($template == 'homepage') {
			echo "<div><a href='/nieuwsarchief' class='blue'>Voor oudere berichten zie <i>Nieuwsarchief</i></a></div>";
		}
	
	}
	wp_reset_postdata();
}
// get the recent post, widget is not sufficient
function main_bib () {
	$post = get_post( 1087 );
	$title = $post->post_title;
	$desc = $post->post_excerpt;
	$link = get_post_permalink( $post );
	echo "<a href=" . $link . ">";
	echo "<div class='newsitem'>";
	echo "<h2>" . $title . "</h2>";
	echo "<p>" . $desc . "</p>";
	echo "<div class='small italic pl1'>";
	echo "lees meer..." ;
	echo "</div>";
	echo "</a></div><hr>";
}

function bib_posts($template) {
	$args = array(
		'post_type' => 'bib',
		'post__not_in' => array(1087),
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC'
	);

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		$count = 0;
		while ( $query->have_posts() ){
			$count++;
			$query->the_post();
			$permalink = get_permalink();
			$date = get_the_date();
			if($template == 'homepage') {
				if($count >= 1 && $count <= 3 ) {
					echo "<div class='newsitem'>";
					if($count == 1 ) {
						if(has_post_thumbnail()) {
							the_post_thumbnail('medium',array('class' => 'ml1'));
						} 
					}
					echo "<a href='". $permalink ."'>";
					if($count == 1 ) {
						echo the_title('<h2>', '</h2>');
					}
					else {
						echo the_title('<h5>', '</h5>');
					}
					echo "<p>" . get_the_excerpt() . "</p>";
					echo "<div class='small italic pl1'>";
					echo "lees meer..." ;
					echo "</div>";
					echo "</a></div><hr>";	
				}
			}
			else if($template == 'archive') {
				echo "<div class='item'>";
				echo "<a href='". $permalink ."'>";
				echo the_title('<h5>', '</h5>');
				echo "<div class='date'>" . $date . "</div>";
				echo "</a>";
				echo "</div>";
			}
		}	
	}
	wp_reset_postdata();
}

function bib_list() {
	$args = array(
		'post_type' => 'bib',
		'post_status' => 'publish',
		'post__not_in' => array(1087),
		'orderby' => 'date',
		'order' => 'DESC'
	);

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		$count = 0;
		while ( $query->have_posts() ){
			$count++;
			$query->the_post();
			echo '<ul>';
			if($count >= 1 && $count <= 3 ) {
				?><li><a href="<?php the_permalink(); ?>"><?php  the_title(); ?></a></li>
			<?php	
		}
			echo '</ul>';
		}
	}
	wp_reset_postdata(); 
}

/* make this function run in a widget */
/* Allow PHP to run in Widgets */
add_filter('widget_text','execute_php_widgets',10);

function execute_php_widgets($html){
   if(strpos($html,"<"."?php")!==false){
   ob_start();
   eval("?".">".$html);
   $html=ob_get_contents();
   ob_end_clean();
   }
return $html;
}

function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );
 

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets'); 
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
 	wp_add_dashboard_widget('custom_help_widget', 'Documentatie', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
	include 'documentatie.txt';
}

// function to get the used page template
add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}
// Disable lazy loading
add_filter('wp_lazy_loading_enabled', '__return_false');


// deactivate new block editor for widgets
function phi_theme_support() {
    remove_theme_support( 'widgets-block-editor' );
}
add_action( 'after_setup_theme', 'phi_theme_support' );

