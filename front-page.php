<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Boerderijenfonds_Theme
 */

get_header();
	$boerderijenfonds_description = get_bloginfo( 'description', 'display' );
?>
<section class="light-green excerpt">
	<div class="section-inner">
		<div class="row">
			<p class="col-12"><?php echo $boerderijenfonds_description; /* WPCS: xss ok. */ ?></p>
		</div>
	</div>
</section>
<section>
	<div class="section-inner">
		<div class="row">
			<div class="col-sm-6 col-xs-12">			
				<div class="home-item" id="aanvragen">
					<a href="/aanvragen">
						<img src="/wp-content/themes/boerderijenfonds/images/FotoAanvragenSmall.jpg" alt="Aanvragen" />
						<span class="inline-title">Aanvragen</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">			
				<div class="home-item" id="projecten">
					<a href="/projecten/">
					<img src="/wp-content/themes/boerderijenfonds/images/FotoProjectenSmall.jpg" alt="Projecten" />
						<span class="inline-title">Projecten</span>
					</a>
				</div>
			</div>
		</div>
		<div class="row">
		
			<div class="col-sm-6 col-xs-12">
				<div class="home-item" id="video">
					<h2>Over het fonds</h2>
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/582529234?h=5d61f6dcb7&title=0&byline=0&portrait=0" 
							style="position:absolute;top:0;left:0;width:100%;height:100%;" 
							frameborder="0" 
							allow="autoplay; fullscreen; picture-in-picture" 
							allowfullscreen>
						</iframe>
					</div>
					<script src="https://player.vimeo.com/api/player.js"></script>
				</div>
				<div id="bib" class="gradient-blue">
							<img src="/wp-content/themes/boerderijenfonds/images/Logo-BiB.png" alt="Boerderij in beweging" />
				<hr>
					<?php main_bib(); ?>
					<?php bib_posts('homepage'); ?>
				</div>
				<div class="center last-image">
					<img  src="/wp-content/themes/boerderijenfonds/images/Beeldmerk-BiB.png" alt="Boerderij in beweging" />
				</div>
			</div>

			<div class="col-sm-6 col-xs-12 gradient">
				<div class="row">
					<div class="col-md-12 wrapper actueel">
						<h1>Actueel</h1>
						<hr>
						<?php nieuws_posts('homepage'); ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</section>
	

<?php
// get_sidebar();
get_footer();
